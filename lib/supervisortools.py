import os
import sys

isSupervised = False
supervisedFd = -1


def _checkSupervisor(fd):
    try:
        os.stat(fd)
        return True
    except:
        return False

if sys.platform.startswith("win"):
    try:
        supervisedFd = os.open(r'\\.\pipe\pstoolspipe', os.O_WRONLY)
        isSupervised = True
    except:
        pass
elif sys.platform.startswith("linux"):
    isSupervised = _checkSupervisor(3)
    if isSupervised:
        supervisedFd = 3
else:
    raise ImportError("this platform is not supported")

if isSupervised:
    def write(s):
        return os.write(supervisedFd, s)
else:
    def write(s):
        return 0