package executor

import (
	"bufio"
	"errors"
	"io"
	"os"
	"os/exec"
	"time"
)

type closeable interface {
	Close() error
}

// ExecCmd executes a given command and channels stdout, stderr, and fd3 for you
// fd3 is implemented platform-specific and is intended as the proper source to get script output
func ExecCmd(stdout chan string, stderr chan string, fd3 chan string, done chan error, name string, arg ...string) {

	cmd := exec.Command(name, arg...)

	/*
	 * agnostic code, linux and win have stdout and stderr
	 */
	stdoutr, stdoutw, err := os.Pipe()
	if err != nil {
		done <- err
		return
	}
	stderrr, stderrw, err := os.Pipe()
	if err != nil {
		done <- err
		return
	}
	cmd.Stdout, cmd.Stderr = stdoutw, stderrw
	doneReading := make(chan error)
	go readerToChannel(stdoutr, stdout, doneReading)
	go readerToChannel(stderrr, stderr, doneReading)

	/*
	 * platform specific, linux uses fd3, windows uses named pipes
	 */
	fd3cancel, err := attachFd3(cmd, fd3, doneReading)
	if err != nil {
		done <- err
		return
	}

	err = cmd.Start()

	doneScripting := make(chan error)
	go func() { doneScripting <- cmd.Wait() }()

	timeout := time.After(20 * time.Second)

	select {
	case <-timeout:
		cmd.Process.Kill()
		stdoutw.Close()
		stderrw.Close()
		fd3cancel.Close()
		done <- errors.New("script timed out")
	case err := <-doneScripting:
		stdoutw.Close()
		stderrw.Close()
		fd3cancel.Close()
		if err != nil {
			done <- err
		}

		for i := 0; i < 3; i++ {
			if err := <-doneReading; err != nil {
				done <- err
				return
			}
		}
		close(doneReading)
		done <- nil
	}
}

func readerToChannel(r io.Reader, lines chan string, done chan error) {
	s := bufio.NewScanner(r)
	s.Split(bufio.ScanLines)
	for s.Scan() {
		lines <- s.Text()
	}
	close(lines)
	done <- nil
}
