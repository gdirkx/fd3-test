package main

import (
	"./executor"
	commonCmd "./executor/cmdprefixes"
	"fmt"
)

func main() {
	fmt.Println("executing", commonCmd.Python, "fd3.py")

	stdout := make(chan string)
	stderr := make(chan string)
	fd3 := make(chan string)
	done := make(chan error)

	go executor.ExecCmd(stdout, stderr, fd3, done, commonCmd.Python, "fd3.py")
	go printLines("stdout", stdout)
	go printLines("stderr", stderr)
	go printLines("fd3", fd3)
	if err := <-done; err != nil {
		panic(err)
	}
}

func printLines(prefix string, lines chan string) {
	for line := range lines {
		fmt.Println(prefix+":", line)
	}
}
