#!/usr/bin/env python3

import os
import time
from lib import supervisortools as sv

if sv.isSupervised:
    os.write(2, b'running under pstools\n')
else:
    os.write(2, b'running standalone\n')

os.write(1, b'fetching data\n')

os.write(1, b'key1: value1\n')
sv.write(b'{"key1": "value1"}\n')

os.write(1, b'sleeping for 1s\n')
time.sleep(1)

os.write(1, b'key2: value2\n')
sv.write(b'{"key2": "value2"}\n')
